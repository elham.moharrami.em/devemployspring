package org.employ.services;

import org.employ.model.Employer;
import org.employ.model.Role;
import org.employ.repositories.EmployerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class EmployerService implements CommandLineRunner {
    private final EmployerRepository employerRepository;

    public EmployerService(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    @Override
    public void run(String... args) {
        Employer employer = new Employer();
        employer.setUsername("microsoft");
        employer.setRole(Role.EMPLOYER);
        employerRepository.save(employer);
        Employer employer1 = new Employer();
        employer1.setUsername("Sony");
        employer1.setRole(Role.EMPLOYER);
        employerRepository.save(employer1);
    }
}
