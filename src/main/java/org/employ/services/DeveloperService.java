package org.employ.services;

import org.employ.model.Developer;
import org.employ.model.Role;
import org.employ.repositories.DeveloperRespository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DeveloperService implements CommandLineRunner {
    private final DeveloperRespository developerRespository;

    public DeveloperService(DeveloperRespository developerRespository) {
        this.developerRespository = developerRespository;
    }


    @Override
    public void run(String... args) {
        Developer developer = new Developer();
        developer.setEmail("eli");
        developer.setPassword(123456L);
        developer.setUsername("elham");
        developer.setRole(Role.DEVELOPER);
        developerRespository.save(developer);

        Developer dev2 = new Developer();
        dev2.setUsername("Ali");
        dev2.setRole(Role.DEVELOPER);
        developerRespository.save(dev2);
    }
}
