package org.employ.services;

import org.employ.model.Project;
import org.employ.repositories.ProjectRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ProjectService implements CommandLineRunner {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Project project = new Project();
        project.setTitle("Ponisha");
        projectRepository.save(project);

        Project project1 = new Project();
        project1.setTitle("Netflix");
        projectRepository.save(project1);
    }
}
