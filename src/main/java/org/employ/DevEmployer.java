package org.employ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevEmployer {
    public static void main(String[] args) {
        SpringApplication.run(DevEmployer.class, args);
    }
}