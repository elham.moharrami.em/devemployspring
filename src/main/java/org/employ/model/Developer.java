package org.employ.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "developer")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Developer extends Account {
    @Column(name = "job_title")
    private String jobTitle;
    @Column(name = "experience")
    private Integer experience;
    @OneToMany(mappedBy = "developer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Project> projects;
}
