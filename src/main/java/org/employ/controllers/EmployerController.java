package org.employ.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Employer;
import org.employ.repositories.EmployerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployerController {
    private static final Logger logger = LogManager.getLogger(EmployerController.class);
    private final EmployerRepository employerRepository;

    public EmployerController(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    @GetMapping("/employers")
    ResponseEntity<List<Employer>> getAll() {
        List<Employer> employers = employerRepository.findAll();
        if (employers.isEmpty()) {
            logger.info("databse is empty. could not get all employers.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(employers, HttpStatus.OK);
    }

    @GetMapping("/employers/{id}")
    ResponseEntity<Employer> findById(@PathVariable("id") Long id) {
        Optional<Employer> employer = employerRepository.findById(id);
        if (employer.isPresent()) {
            return new ResponseEntity<>(employer.get(), HttpStatus.OK);
        } else {
            logger.info("could not find requested employer.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/employers")
    ResponseEntity<Employer> add(@RequestBody Employer employerBody) {
        try {
            Employer employer = new Employer();
            employer.setUsername(employerBody.getUsername());
            employer.setId(employerBody.getId());
            employer.setEmail(employerBody.getEmail());
            employer.setPassword(employerBody.getPassword());
            employer.setContact(employerBody.getContact());
            employer.setProjects(employerBody.getProjects());
            employer.setRole(employerBody.getRole());
            employerRepository.save(employer);
            return new ResponseEntity<>(employer, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("could not add employer" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/employers/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable Long id) {
        try {
            employerRepository.deleteById(id);
            System.out.println("deleted" + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            logger.warn("could not delete employer." + e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
