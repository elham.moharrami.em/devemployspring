package org.employ.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Project;
import org.employ.repositories.ProjectRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProjectController {
    private static final Logger logger = LogManager.getLogger(ProjectController.class);
    private final ProjectRepository projectRepository;

    public ProjectController(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @GetMapping("/projects")
    ResponseEntity<List<Project>> getAll() {
        List<Project> projects = projectRepository.findAll();
        if (projects.isEmpty()) {
            logger.info("database is empty. could not get all projects.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping("/projects/{id}")
    ResponseEntity<Project> findById(@PathVariable("id") Long id) {
        Optional<Project> project = projectRepository.findById(id);
        if (project.isPresent()) {
            return new ResponseEntity<>(project.get(), HttpStatus.OK);
        } else {
            logger.info("could not get the requested project.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/projects")
    ResponseEntity<Project> add(@RequestBody Project projectBody) {
        try {
            Project project = new Project();
            project.setId(projectBody.getId());
            project.setTitle(projectBody.getTitle());
            project.setProjectStatus(projectBody.getProjectStatus());
            project.setDeveloper(projectBody.getDeveloper());
            project.setEmployer(projectBody.getEmployer());
            project.setPrice(projectBody.getPrice());
            project.setStartDate(projectBody.getStartDate());
            project.setEndDate(projectBody.getEndDate());
            projectRepository.save(project);
            return new ResponseEntity<>(project, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("could not add project." + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/projects/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable Long id) {
        try {
            projectRepository.deleteById(id);
            System.out.println("deleted" + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            logger.warn("could not delete project." + e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
