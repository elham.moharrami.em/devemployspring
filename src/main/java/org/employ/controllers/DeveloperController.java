package org.employ.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Developer;
import org.employ.repositories.DeveloperRespository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class DeveloperController {
    private static final Logger logger = LogManager.getLogger(DeveloperController.class);
    private final DeveloperRespository developerRespository;

    public DeveloperController(DeveloperRespository developerRespository) {
        this.developerRespository = developerRespository;
    }

    @GetMapping("/developers")
    ResponseEntity<List<Developer>> all() {
        List<Developer> developers = developerRespository.findAll();
        if (developers.isEmpty()) {
            logger.info("database is empty.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(developers, HttpStatus.OK);
    }

    @GetMapping("/developers/{id}")
    public ResponseEntity<Developer> getById(@PathVariable("id") long id) {
        Optional<Developer> developerData = developerRespository.findById(id);
        if (developerData.isPresent()) {
            return new ResponseEntity<>(developerData.get(), HttpStatus.OK);
        } else {
            logger.info("could not find requested developer.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/developers", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Developer> createDeveloper(@RequestBody Developer developerBody) {
        try {
            Developer developer = new Developer();
            developer.setUsername(developerBody.getUsername());
            developer.setId(developerBody.getId());
            developer.setEmail(developerBody.getEmail());
            developer.setContact(developerBody.getContact());
            developer.setPassword(developerBody.getPassword());
            developer.setExperience(developerBody.getExperience());
            developer.setJobTitle(developerBody.getJobTitle());
            developer.setProjects(developer.getProjects());
            developer.setRole(developerBody.getRole());
            return new ResponseEntity<>(developer, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("could not add developer to the database." + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/developers/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable Long id) {
        try {
            developerRespository.deleteById(id);
            System.out.println("deleted" + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            logger.warn("could not delete developer." + e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
