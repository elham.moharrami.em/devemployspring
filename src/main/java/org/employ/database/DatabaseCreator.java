package org.employ.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.common.ConfigProperties;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

@Component
public class DatabaseCreator implements CommandLineRunner {
    private final ConfigProperties configProperties;
    private static final Logger logger = LogManager.getLogger(DatabaseCreator.class);

    public DatabaseCreator(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    public void createDatabase() {
        String dbName = configProperties.getName();
        boolean exists = checkDatabaseExist();
        try (Connection connection = DriverManager.getConnection(configProperties.getRawurl(),
                configProperties.getUsername(), configProperties.getPassword());
             Statement statement = connection.createStatement();) {
            if (!exists) {
                String createDatabaseQuery = "CREATE DATABASE " + dbName;
                statement.executeUpdate(createDatabaseQuery);
                logger.info("Database " + dbName + " created successfully.");
            } else {
                logger.info("database already exists.");
            }
        } catch (Exception e) {
            logger.error("couldn't create database." + e.getMessage());
        }
    }

    private boolean checkDatabaseExist() {
        try(Connection connection = DriverManager.getConnection(configProperties.getUrl(),
                configProperties.getUsername(), configProperties.getPassword());) {
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void run(String... args) throws Exception {
        createDatabase();
    }
}
