package org.employ.repositories;

import org.employ.model.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeveloperRespository extends JpaRepository<Developer, Long> {
}
