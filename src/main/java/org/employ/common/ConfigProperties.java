package org.employ.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.datasource")
@Component
public class ConfigProperties {
    private String rawurl;
    private String url;
    private String username;
    private String password;
    private String name;
}
